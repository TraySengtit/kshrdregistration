import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Home from '../screens/Home';
import {Image, Text} from 'native-base';
import {View} from 'react-native';

const Stack = createStackNavigator();

const MyStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={Home}
        options={{
          headerMode: null,
          headerLeft: () => (
            <Image
              ml={7}
              mt={-10}
              mb={2}
              size={20}
              resizeMode={'contain'}
              source={{
                uri: 'https://kshrd.com.kh/static/media/logo.f368c431.png',
              }}
              alt="KSHRD"
            />
          ),
          headerStatusBarHeight: 40,
          headerTitle: () => (
            <View
              style={{
                height: 40,
                marginBottom: 47,
                // marginLeft: 25,
                // width: '80%',
              }}>
              <Text>Korea Software HRD Center</Text>
              <Text>មជ្ឈមណ្ឌលកូរ៉េ​ សហ្វវែរ អេច អ​​ ឌី</Text>
            </View>
          ),
        }}
      />
    </Stack.Navigator>
  );
};
export default MyStack;
