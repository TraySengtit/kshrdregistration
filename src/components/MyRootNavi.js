import {NavigationContainer} from '@react-navigation/native';
import React from 'react';
import SafeAreaView from 'react-native-safe-area-view';
import MyStack from './MyStack';

const MyRootNavi = () => {
  return (
    <SafeAreaView style={{flex: 1}} >
      <NavigationContainer>
        <MyStack/>
      </NavigationContainer>
    </SafeAreaView>
  );
};

export default MyRootNavi;
