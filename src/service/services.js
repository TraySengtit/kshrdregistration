import { Platform } from "react-native";
import { api } from "../utils/api";

export const get_All_Provinces = async () => {
    try {
        const province = await api.get('provinces');
        return province.data
    } catch (error) {
        console.log("ERROR :", error);
    }
}
export const get_All_Universities = async () => {
    try {
        const university = await api.get('universities');
        return university.data
    } catch (error) {
        console.log("ERROR :", error);
    }
}
export const register_student = async (register) => {
    const postStudent = await api.post('students', register)
    console.log("Post Student : ", postStudent.data)
    return postStudent.data
}

export const imageUpload = async (image) => {
    try {
        const imagePath = `${image.path}`.split('/');
        const imgName = imagePath[imagePath.length - 1];
        const formData = new FormData();
        formData.append('file', {
            uri: image.path,
            name: Platform.OS === "ios" ? image.filename : imgName,
            type: image.mime
        })
        const res = await api.post('files/upload', formData);
        console.log("Upload image success :", res.data)
        return res.data
    } catch (error) {
        console.log("Upload image error :", error);
    }
}
