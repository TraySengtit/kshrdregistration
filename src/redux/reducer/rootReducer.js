import { combineReducers } from "redux";
import { provinceReducer } from './ProvinceReducer';
import {universityReducer} from './UniversityReducer'

export const rootReducer = combineReducers({
    provinceReducer, universityReducer

})