
import { GET_ALL_PROVINCES } from "../actions/ProvicesAction";

const initialState = {
    province: []
}


export const provinceReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ALL_PROVINCES:
                return{
                    ...state,
                    province: action.payload
                }
                break;
        default: return state;
        break;
    }
}