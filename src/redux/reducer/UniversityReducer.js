import { GET_ALL_UNIVERSITIES } from "../actions/UniversityAction";

const initialState = {
    university: []
}


export const universityReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ALL_UNIVERSITIES:
                return{
                    ...state,
                    university: action.payload
                }
                
        default: return state;
    }
}