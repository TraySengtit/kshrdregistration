
import { get_All_Universities } from '../../service/services';

export const GET_ALL_UNIVERSITIES = "GET_ALL_UNIVERSITIES";

export const getAllUniversities = () => async (dispatch) => {
    const university = await get_All_Universities()
    dispatch({
        type: GET_ALL_UNIVERSITIES,
        payload: university.payload
    })
}