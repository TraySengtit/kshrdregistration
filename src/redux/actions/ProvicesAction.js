
import { get_All_Provinces } from '../../service/services';

export const GET_ALL_PROVINCES = "GET_ALL_PROVINCES";

export const getAllProvinces = () => async (dispatch) => {
    const provinces = await get_All_Provinces()
    dispatch({
        type: GET_ALL_PROVINCES,
        payload: provinces.payload
    })
}