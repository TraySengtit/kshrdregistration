import React, { useState } from "react"
import {
    Select,
    VStack,
    CheckIcon,
    Center,
    Actionsheet,
    useDisclose, Button, Text
} from "native-base"
import  ImagePickerCropper from 'react-native-image-crop-picker';
import { ImageBackground, StyleSheet, TouchableOpacity, View } from "react-native";

import BottomSheet from 'reanimated-bottom-sheet';
import Animated from 'react-native-reanimated';
import { useTheme } from 'react-native-paper';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import { red100 } from "react-native-paper/lib/typescript/styles/colors";

export const ImageChoose = () => {
    const [imagePath, setImagePath] = useState('https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png')
    // const { colors } = useTheme();
    const takePhotoFromCamera = () => {
        ImagePickerCropper.openCamera({
            width: 300,
            height: 300,
            cropping: true,
            freeStyleCropEnabled: true,
        }).then(image => {
            console.log(image);
            setImagePath(image.path);
            bs.current.snapTo(1);
        });
    }

    const choosePhotoFromLibrary = () => {
        ImagePickerCropper.openPicker({
            width: 300,
            height: 300,
            cropping: true,
            freeStyleCropEnabled: true,
        }).then(image => {
            console.log(image);
            setImagePath(image.path);
            bs.current.snapTo(1);
        });
    }

    renderInner = () => (
        <View style={styles.panel}>
            <View style={{ alignItems: 'center' , height: '15%'}}>
                <Text style={styles.panelTitle}>Choose your Photo</Text>
            </View>
            <TouchableOpacity style={styles.panelButton} onPress={takePhotoFromCamera}>
                <Text style={styles.panelButtonTitle}>Take Photo</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.panelButton} onPress={choosePhotoFromLibrary}>
                <Text style={styles.panelButtonTitle}>Choose From Library</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={styles.panelButton}
                onPress={() => bs.current.snapTo(1)}
            >
                <Text style={styles.panelButtonTitle}>Cancel</Text>
            </TouchableOpacity>
        </View>
    );

    renderHeader = () => (
        <View style={styles.header}>
        <TouchableOpacity onPress={() => bs.current.snapTo(1)}>
            <View style={styles.panelHeader}>
                <View style={styles.panelHandle} />
            </View>
            </TouchableOpacity>
        </View>
    );

    bs = React.createRef();
    fall = new Animated.Value(1);

    return (
        <View style={styles.container}>
            <BottomSheet
                // style={{marginTop: 100}}
                ref={bs}
                // headerPosition={}
                topI
                snapPoints={['33%', 0]}
                renderContent={renderInner}
                renderHeader={renderHeader}
                initialSnap={1}
                callbackNode={fall}
                enabledGestureInteraction={true}
                // enabledContentGestureInteraction={false}
            />
            <Animated.View style={{
                margin: 30, marginTop: 100,
                opacity: Animated.add(0.3, Animated.multiply(fall, 3)),
            }}>
                <View style={{ alignItems: 'center' ,justifyContent: 'center'}}>
                    <TouchableOpacity onPress={() => bs.current.snapTo(0)}>
                        <View
                            style={{
                                height: 100,
                                width: 100,
                                borderRadius: 15,
                                justifyContent: 'center',
                                alignItems: 'center',
                            }}
                            >
                            <ImageBackground
                                source={{
                                    uri: imagePath,
                                }}
                                style={{ height: 200, width: 200 , overflow: 'hidden'}}
                                imageStyle={{ borderRadius: 100 }}>
                                {/* <View
                                    style={{
                                        flex: 1,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}>
                                    <Icon
                                        name="camera"
                                        size={35}
                                        color="#fff"
                                        style={{
                                            opacity: 0.7,
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                            borderWidth: 1,
                                            borderColor: '#fff',
                                            borderRadius: 10,
                                        }}
                                    />
                                </View> */}
                            </ImageBackground>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => bs.current.snapTo(0)} style={{ marginTop: 35, fontSize: 18, backgroundColor: 'rgba(255, 255, 255, 0.8)', fontWeight: 'bold', borderRadius: 20, padding: 10 }}>
                        <Text>Choose Image</Text>
                    </TouchableOpacity>
                </View>
            </Animated.View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center'
    },
    commandButton: {
        padding: 15,
        borderRadius: 10,
        backgroundColor: '#FF6347',
        alignItems: 'center',
        marginTop: 10,
    },
    panel: {
        padding: 20,
        backgroundColor: '#FFFFFF',
        paddingTop: 20,
        // borderTopLeftRadius: 20,
        // borderTopRightRadius: 20,
        shadowColor: '#000000',
        shadowOffset: {width: 0, height: 0},
        shadowRadius: 5,
        shadowOpacity: 0.4,
    },
    header: {
        width: '100%',
        backgroundColor: '#FFFFFF',
        shadowColor: '#333333',
        shadowOffset: { width: -4, height: -10 },
        shadowRadius: 2,
        shadowOpacity: 0.4,
        elevation: 2,
        paddingTop: 10,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    panelHeader: {
        alignItems: 'center',
        // marginTop: 100
        // paddingTop: 10
    },
    panelHandle: {
        width: 40,
        height: 8,
        borderRadius: 4,
        backgroundColor: '#00000040',
        marginBottom: 6
    },
    panelTitle: {
        // marginTop: 5,
        fontSize: 22,
        height: 30,
    },
    panelSubtitle: {
        fontSize: 14,
        color: 'gray',
        height: 30,
        marginBottom: 10,
    },
    panelButton: {
        padding: 13,
        borderRadius: 10,
        backgroundColor: '#FF6347',
        alignItems: 'center',
        marginVertical: 7,
    },
    panelButtonTitle: {
        fontSize: 17,
        fontWeight: 'bold',
        color: 'white',
    },
    action: {
        flexDirection: 'row',
        marginTop: 10,
        marginBottom: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5,
    },
    actionError: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#FF0000',
        paddingBottom: 5,
    },
    textInput: {
        flex: 1,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: '#05375a',
    },
});