import {
    Button,
    Center,
    FormControl,
    Input,
    Actionsheet,
    ScrollView,
    useDisclose,
    Text,
    Radio,
    HStack,
    Select,
    CheckIcon,
    Box,
    VStack,
} from 'native-base';
import React, { useEffect, useState } from 'react';
import {
    View,
    StyleSheet,
    Platform,
    TouchableOpacity,
    TouchableWithoutFeedback,
    ImageBackground,
    Alert,
} from 'react-native';
import { Formik } from 'formik';
import * as Yup from 'yup';
import DatePicker from 'react-native-date-picker';
import moment, { now } from 'moment';
import { useDispatch, useSelector } from 'react-redux';
import { getAllProvinces } from '../redux/actions/ProvicesAction';
import { getAllUniversities } from '../redux/actions/UniversityAction';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { ImageChoose } from './ImageChoose';
import BottomSheet from 'reanimated-bottom-sheet';
import Animated, { log } from 'react-native-reanimated';
import ImagePickerCropper from 'react-native-image-crop-picker';
import { imageUpload, register_student } from './../service/services';

const Home = () => {
    const dispatch = useDispatch();
    const { province } = useSelector(state => state.provinceReducer);
    const { university } = useSelector(state => state.universityReducer);

    useEffect(() => {
        dispatch(getAllProvinces());
        dispatch(getAllUniversities());
    }, []);

    const [isPickerShow, setIsPickerShow] = useState(false);
    const [date, setDate] = useState(new Date());
    const monthNames = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
    ];

    // useEffect(() => {
    //     setDob
    // }, [])
    
    const birthDate = `${date.getDate()}${' / '}${monthNames[date.getMonth()]}${' / '}${date.getFullYear()}`;
    // const birthDate = `${date.toLocaleDateString()}`;
    console.log('THIS IS BRITH DATE : ', birthDate);
    console.log('THIS IS BRITH DATE 22222: ', birthDate);
    const [dob, setDob] = useState(birthDate);
    console.log('THIS IS BRITH DATE 33333: ', dob);


    const showPicker = () => {
        setIsPickerShow(true);
    };

    const [photo, setPhoto] = useState('');
    console.log("ASDHIASDHIJAS", photo);
    const [imagePath, setImagePath] = useState(
        'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png',
    );
    const takePhotoFromCamera = () => {
        ImagePickerCropper.openCamera({
            width: 300,
            height: 300,
            cropping: true,
            freeStyleCropEnabled: true,
        }).then(image => {
            console.log(image);
            setImagePath(image.path);
            setPhoto(image);
            bs.current.snapTo(1);
        });
    };

    const choosePhotoFromLibrary = () => {
        ImagePickerCropper.openPicker({
            width: 300,
            height: 300,
            cropping: true,
            freeStyleCropEnabled: true,
        }).then(image => {
            console.log('THIS IS IMAGE', image);
            setImagePath(image.path);
            setPhoto(image);
            bs.current.snapTo(1);
        });
    };

    renderInner = () => (
        <View style={styles.panel}>
            <View style={{ alignItems: 'center', height: '15%' }}>
                <Text style={styles.panelTitle}>Choose your Photo</Text>
            </View>
            <TouchableOpacity
                style={styles.panelButton}
                onPress={takePhotoFromCamera}>
                <Text style={styles.panelButtonTitle}>Take Photo</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={styles.panelButton}
                onPress={choosePhotoFromLibrary}>
                <Text style={styles.panelButtonTitle}>Choose From Library</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={styles.panelButton}
                onPress={() => bs.current.snapTo(1)}>
                <Text style={styles.panelButtonTitle}>Cancel</Text>
            </TouchableOpacity>
        </View>
    );

    renderHeader = () => (
        <>
            <TouchableWithoutFeedback onPress={() => bs.current.snapTo(1)}>
                <View style={styles.header}>
                    <TouchableOpacity onPress={() => bs.current.snapTo(1)}>
                        <View style={styles.panelHeader}>
                            <View style={styles.panelHandle} />
                        </View>
                    </TouchableOpacity>
                </View>
            </TouchableWithoutFeedback>
        </>
    );

    bs = React.createRef();
    fall = new Animated.Value(1);
    return (
        <>
            <BottomSheet
                ref={bs}
                topI
                snapPoints={['30%', 0]}
                renderContent={renderInner}
                renderHeader={renderHeader}
                initialSnap={1}
                callbackNode={fall}
                enabledGestureInteraction={true}
            />
            <Animated.View
                style={{
                    opacity: Animated.add(0.3, Animated.multiply(fall, 3)),
                }}>
                <ScrollView>
                    <Center>
                        <Formik
                            initialValues={{
                                fullnameInLatin: '',
                                email: '',
                                gender: 'male',
                                phoneNumber: '',
                                universityState: '',
                                placeOfBirth: '',
                                currentAddress: '',
                                dob: new Date(),
                                emergencyContact: '',
                                relationship: '',
                                education: '',
                            }}
                            validationSchema={Yup.object({
                                fullnameInLatin: Yup.string().required(
                                    'Please enter your name in English',
                                ),
                                gender: Yup.string().required('Please choose gender'),
                                email: Yup.string()
                                    .email('Invalid Email')
                                    .required('Please enter your email'),
                                phoneNumber: Yup.string().required(
                                    'Please enter your phone number',
                                ),
                                universityState: Yup.string().required(
                                    'Please choose university',
                                ),
                                placeOfBirth: Yup.string().required(
                                    'Please choose place of birth',
                                ),
                                currentAddress: Yup.string().required(
                                    'Please choose current address',
                                ),
                                dob: Yup.date().required('Please select a date'),
                                emergencyContact: Yup.string().required(
                                    'Please enter emergency contact',
                                ),
                                relationship: Yup.string().required(
                                    'Please select relationship',
                                ),
                                education: Yup.string().required(
                                    'Please select your education',
                                ),
                            })}
                            onSubmit={(values, { setFormValues }) => {
                                if (imagePath) {

                                    imageUpload(photo).then((val)=>{
                                        
                                        console.log("UP IMAGE : ", val);
                                        const currentAddressId = province.filter(
                                            item => item.fullname == values.currentAddress,
                                        );
                                        const placeOfBirthId = province.filter(
                                            item => item.fullname == values.placeOfBirth,
                                        );
                                        const universityId = university.filter(
                                            item => item.fullname == values.universityState,
                                        );
    
                                        const student = {
                                            currentAddressId: currentAddressId[0].id,
                                            dateOfBirth: dob,
                                            education: values.education,
                                            email: values.email,
                                            emergencyContact: values.emergencyContact,
                                            emergencyContactRelationship:
                                                values.relationship.toUpperCase(),
                                            fullname: values.fullnameInLatin,
                                            gender: values.gender.toUpperCase(),
                                            nationalId: '',
                                            phone: values.phoneNumber,
                                            "photo": `${val.fullpath}`,
                                            placeOfBirthId: placeOfBirthId[0].id,
                                            question: '',
                                            universityId: universityId[0].id,
                                        };
                                        register_student(student)
                                            .then(res => {
                                                Alert.alert('Register successfully.', res);
                                            })
                                            .catch(error => {
                                                Alert.alert('Failed to register!!!', error);
                                            });
                                        setFormValues({ values: '' });
                                    })
                                } else {
                                    Alert.alert('Please choose image.');
                                }
                            }}>
                            {props =>
                                console.log(props) || (
                                    <FormControl
                                        w={{
                                            base: '83%',
                                        }}>
                                        <Text
                                            mt={2}
                                            fontSize="2xl"
                                            color="#6495ED"
                                            fontWeight="medium">
                                            Application Form {'\n'}
                                            <Text mt={2} fontSize="14" fontWeight="medium">
                                                Please complete all your information
                                            </Text>
                                        </Text>

                                        {/***************** English name  ****************/}
                                        <FormControl.Label mt={'2.5'}>Full Name</FormControl.Label>
                                        <Input
                                            value={props.values.fullnameInLatin}
                                            onChangeText={props.handleChange('fullnameInLatin')}
                                            onBlur={props.handleBlur('fullnameInLatin')}
                                            placeholder="Enter English Name"
                                            mb={2}
                                        />
                                        {props.touched.fullnameInLatin &&
                                            props.errors.fullnameInLatin ? (
                                            <Text style={styles.error}>
                                                {props.errors.fullnameInLatin}
                                            </Text>
                                        ) : null}

                                        {/***************** EMAIL  ****************/}
                                        <FormControl.Label mt={'2.5'}>
                                            Email Address
                                        </FormControl.Label>
                                        <Input
                                            value={props.values.email}
                                            onChangeText={props.handleChange('email')}
                                            onBlur={props.handleBlur('email')}
                                            placeholder="Enter your email"
                                            mb={2}
                                            keyboardType="email-address"
                                        />
                                        {props.touched.email && props.errors.email ? (
                                            <Text style={styles.error}>{props.errors.email}</Text>
                                        ) : null}

                                        {/***************** Radio  ****************/}
                                        <FormControl.Label mt={'2.5'}>Gender</FormControl.Label>
                                        <Radio.Group
                                            onChange={props.handleChange('gender')}
                                            value={props.values.gender}>
                                            <HStack space={2}>
                                                <Radio colorScheme="blue" value="male" ml={'0.5'}>
                                                    Male
                                                </Radio>
                                                <Radio colorScheme="blue" value="female">
                                                    Female
                                                </Radio>
                                                {props.touched.gender && props.errors.gender ? (
                                                    <Text style={styles.error}>
                                                        {props.errors.gender}
                                                    </Text>
                                                ) : null}
                                            </HStack>
                                        </Radio.Group>

                                        <FormControl.Label mt={'4'}>
                                            Choose Date of Birth
                                        </FormControl.Label>

                                        {/***************** BirthDate  ****************/}
                                        <Button
                                            borderRadius="5"
                                            h="10"
                                            style={styles.pickedDateContainer}
                                            onPress={showPicker}
                                            variant="unstyled">
                                            <View style={styles.clock}>
                                                {Platform.OS === 'android' ? (
                                                    <HStack w="80" justifyContent="space-between">
                                                        <Text ml={1}>{birthDate}</Text>
                                                        <Icon
                                                            name="calendar-month-outline"
                                                            size={25}
                                                            color="gray"
                                                            style={{
                                                                marginRight: 5,
                                                                opacity: 0.7,
                                                                alignItems: 'center',
                                                                justifyContent: 'center',
                                                                borderColor: '#fff',
                                                                borderRadius: 10,
                                                            }}
                                                        />
                                                    </HStack>
                                                ) : (
                                                    <HStack w="80" justifyContent="space-between">
                                                        <Text ml="1.5">{birthDate}</Text>
                                                        <Text mr="1.5">{''}</Text>
                                                    </HStack>
                                                )}
                                            </View>
                                        </Button>
                                        {isPickerShow && (
                                            <DatePicker
                                                androidVariant="iosClone"
                                                value={props.values.dob}
                                                modal
                                                mode="date"
                                                open={isPickerShow}
                                                date={date}
                                                onDateChange={props.values.dob}
                                                onConfirm={date => {
                                                    setIsPickerShow(false);
                                                    setDate(date);
                                                    setDob(date);
                                                }}
                                                onCancel={() => {
                                                    setIsPickerShow(false);
                                                }}
                                            />
                                        )}
                                        {props.touched.dob && props.errors.dob ? (
                                            <Text style={styles.error}>{props.errors.dob}</Text>
                                        ) : null}

                                        {/***************** Choose Place of Birth  ****************/}
                                        <FormControl.Label mt={4}>
                                            Choose Place of Birth
                                        </FormControl.Label>
                                        <Select
                                            // selectedValue={placeOfBirth}
                                            value={props.values.placeOfBirth}
                                            minWidth="200"
                                            accessibilityLabel="Place of Birth"
                                            placeholder="Place of Birth"
                                            _selectedItem={{
                                                bg: 'info.400',
                                                endIcon: <CheckIcon size="5" />,
                                            }}
                                            mt={1}
                                            onValueChange={props.handleChange('placeOfBirth')}>
                                            {province.map(value => (
                                                <Select.Item
                                                    key={value.id}
                                                    label={value.fullname}
                                                    value={value.fullname}
                                                />
                                            ))}
                                        </Select>
                                        {props.touched.placeOfBirth && props.errors.placeOfBirth ? (
                                            <Text style={styles.error}>
                                                {props.errors.placeOfBirth}
                                            </Text>
                                        ) : null}
                                        {/***************** Choose Current Address  ****************/}
                                        <FormControl.Label mt={3}>
                                            Choose Current Address
                                        </FormControl.Label>
                                        <Select
                                            value={props.values.currentAddress}
                                            minWidth="200"
                                            accessibilityLabel="Current Address"
                                            placeholder="Current Address"
                                            _selectedItem={{
                                                bg: 'info.400',
                                                endIcon: <CheckIcon size="5" />,
                                            }}
                                            onValueChange={props.handleChange('currentAddress')}>
                                            {province.map(value => (
                                                <Select.Item
                                                    key={value.id}
                                                    label={value.fullname}
                                                    value={value.fullname}
                                                />
                                            ))}
                                        </Select>
                                        {props.touched.currentAddress &&
                                            props.errors.currentAddress ? (
                                            <Text style={styles.error}>
                                                {props.errors.currentAddress}
                                            </Text>
                                        ) : null}

                                        {/***************** University  ****************/}
                                        <FormControl.Label mt={3}>University</FormControl.Label>
                                        <Select
                                            value={props.values.universityState}
                                            minWidth="200"
                                            name
                                            accessibilityLabel="University"
                                            placeholder="Choose University"
                                            _selectedItem={{
                                                bg: 'info.300',
                                                endIcon: <CheckIcon size="5" />,
                                            }}
                                            onValueChange={props.handleChange('universityState')}>
                                            {university.map(value => (
                                                <Select.Item
                                                    key={value.id}
                                                    label={value.fullname}
                                                    value={value.abbreviation}
                                                />
                                            ))}
                                        </Select>
                                        {props.touched.universityState &&
                                            props.errors.universityState ? (
                                            <Text style={styles.error}>
                                                {props.errors.universityState}
                                            </Text>
                                        ) : null}

                                        {/***************** Phone Number  ****************/}
                                        <FormControl.Label mt={'2.5'}>
                                            Phone Number
                                        </FormControl.Label>
                                        <Input
                                            value={props.values.phoneNumber}
                                            onChangeText={props.handleChange('phoneNumber')}
                                            onBlur={props.handleBlur('phoneNumber')}
                                            placeholder="Enter your phone number"
                                            mb={2}
                                            keyboardType="numeric"
                                        />
                                        {props.touched.phoneNumber && props.errors.phoneNumber ? (
                                            <Text style={styles.error}>
                                                {props.errors.phoneNumber}
                                            </Text>
                                        ) : null}
                                        {/***************** EMERGENCY CONTACT  ****************/}
                                        <FormControl.Label mt={'2.5'}>
                                            EMERGENCY CONTACT
                                        </FormControl.Label>
                                        <Input
                                            value={props.values.emergencyContact}
                                            onChangeText={props.handleChange('emergencyContact')}
                                            onBlur={props.handleBlur('emergencyContact')}
                                            placeholder="Enter your emergency number"
                                            mb={2}
                                            keyboardType="numeric"
                                        />
                                        {props.touched.emergencyContact &&
                                            props.errors.emergencyContact ? (
                                            <Text style={styles.error}>
                                                {props.errors.emergencyContact}
                                            </Text>
                                        ) : null}
                                        {/***************** RELATIONSHIP  ****************/}
                                        <FormControl.Label mt={3}>RELATIONSHIP</FormControl.Label>
                                        <Select
                                            value={props.values.relationship}
                                            minWidth="200"
                                            name
                                            accessibilityLabel="relationship"
                                            placeholder="Choose relationship"
                                            _selectedItem={{
                                                bg: 'info.300',
                                                endIcon: <CheckIcon size="5" />,
                                            }}
                                            onValueChange={props.handleChange('relationship')}>
                                            <Select.Item label="Father" value="FATHER" />
                                            <Select.Item label="Mother" value="MOTHER" />
                                            <Select.Item label="Brother" value="BROTHER" />
                                            <Select.Item label="Sister" value="SISTER" />
                                            <Select.Item label="Uncle" value="UNCLE" />
                                            <Select.Item label="Aunt" value="AUNT" />
                                            <Select.Item label="Other" value="OTHER" />
                                        </Select>
                                        {props.touched.relationship && props.errors.relationship ? (
                                            <Text style={styles.error}>
                                                {props.errors.relationship}
                                            </Text>
                                        ) : null}
                                        {/***************** EDUCATIION  ****************/}
                                        <FormControl.Label mt={3}>EDUCATION</FormControl.Label>
                                        <Select
                                            value={props.values.education}
                                            minWidth="200"
                                            name
                                            accessibilityLabel="education"
                                            placeholder="Select your education"
                                            _selectedItem={{
                                                bg: 'info.300',
                                                endIcon: <CheckIcon size="5" />,
                                            }}
                                            onValueChange={props.handleChange('education')}>
                                            <Select.Item
                                                label="YEAR3_SEMESTER1"
                                                value="YEAR3_SEMESTER1"
                                            />
                                            <Select.Item
                                                label="YEAR3_SEMESTER2"
                                                value="YEAR3_SEMESTER2"
                                            />
                                            <Select.Item
                                                label="YEAR4_SEMESTER1"
                                                value="YEAR4_SEMESTER1"
                                            />
                                            <Select.Item
                                                label="YEAR4_SEMESTER2"
                                                value="YEAR4_SEMESTER2"
                                            />
                                            <Select.Item label="GRATUATED" value="GRATUATED" />
                                        </Select>
                                        {props.touched.education && props.errors.education ? (
                                            <Text style={styles.error}>{props.errors.education}</Text>
                                        ) : null}

                                        <View
                                            style={{
                                                margin: 90,
                                                alignItems: 'center',
                                                justifyContent: 'center',
                                                marginBottom: 20,
                                            }}>
                                            <TouchableOpacity onPress={() => bs.current.snapTo(0)}>
                                                <View
                                                    style={{
                                                        height: 100,
                                                        width: 100,
                                                        borderRadius: 15,
                                                        justifyContent: 'center',
                                                        alignItems: 'center',
                                                    }}>
                                                    <ImageBackground
                                                        source={{
                                                            uri: imagePath,
                                                        }}
                                                        style={{
                                                            height: 230,
                                                            width: 230,
                                                            overflow: 'hidden',
                                                        }}
                                                        imageStyle={{ borderRadius: 120 }}></ImageBackground>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity
                                                onPress={() => bs.current.snapTo(0)}
                                                style={{
                                                    marginTop: 50,
                                                    fontSize: 18,
                                                    backgroundColor: 'rgba(255, 255, 255, 0.8)',
                                                    fontWeight: 'bold',
                                                    borderRadius: 20,
                                                    padding: 10,
                                                }}>
                                                <Text>Choose Image</Text>
                                            </TouchableOpacity>
                                        </View>

                                        <Button
                                            bg="#2e46ff"
                                            w="full"
                                            mt={'2.5'}
                                            onPress={props.handleSubmit}
                                            loading={props.isSubmitting}>
                                            Register
                                        </Button>
                                        <Button
                                            bg="#2e46ff"
                                            w="full"
                                            mt={5}
                                            mb={5}
                                            onPress={props.handleReset}>
                                            clear
                                        </Button>
                                    </FormControl>
                                )
                            }
                        </Formik>
                    </Center>
                </ScrollView>
            </Animated.View>
        </>
    );
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
        
    },
    error: {
        color: 'red',
    },
    pickedDateContainer: {
        marginTop: 7,
        width: '100%',
        textAlign: 'right',
        
        borderWidth: 0.2,
        color: 'black',
        borderColor: '#CBCBCB',
    },
    pickedDate: {
        fontSize: 18,
        
    },
    datePicker: {
        width: 320,
        height: 260,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    commandButton: {
        padding: 15,
        borderRadius: 10,
        backgroundColor: '#FF6347',
        alignItems: 'center',
        marginTop: 10,
    },
    panel: {
        padding: 20,
        backgroundColor: '#FFFFFF',
        paddingTop: 20,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 5,
        shadowOpacity: 0.4,
    },
    header: {
        width: '100%',
        backgroundColor: '#FFFFFF',
        elevation: 2,
        paddingTop: 10,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    panelHeader: {
        alignItems: 'center',
    },
    panelHandle: {
        width: 40,
        height: 8,
        borderRadius: 4,
        backgroundColor: '#00000040',
        marginBottom: 6,
    },
    panelTitle: {
        fontSize: 22,
        height: 30,
    },
    panelSubtitle: {
        fontSize: 14,
        color: 'gray',
        height: 30,
        marginBottom: 10,
    },
    panelButton: {
        padding: 13,
        borderRadius: 10,
        backgroundColor: '#2e46ff',
        alignItems: 'center',
        marginVertical: 7,
    },
    panelButtonTitle: {
        fontSize: 17,
        fontWeight: 'bold',
        color: 'white',
    },
    
});
export default Home;
