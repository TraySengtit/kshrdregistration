
import { NativeBaseProvider } from 'native-base';
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import 'react-native-gesture-handler';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Provider } from 'react-redux';
import MyRootNavi from './src/components/MyRootNavi';
import { store } from './src/redux/store/store';
import { ImageChoose } from './src/screens/ImageChoose';

const App = () => {
  
  return (
    <Provider store={store}>
      <SafeAreaProvider>
        <View style={styles.container}>
          <NativeBaseProvider>
            <MyRootNavi />
            {/* <ImageChoose/> */}
          </NativeBaseProvider>
        </View>
      </SafeAreaProvider>
    </Provider>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%'
  },
  error: {
    color: 'red'
  }
})
export default App;
